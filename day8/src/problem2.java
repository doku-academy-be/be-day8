import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class problem2 {
    public static void main(String[] args) {
        int jumlah = 432;
        int total =0;

        List<Integer> koin = new ArrayList<>(List.of(1, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000));
        List<Integer> dompet = new ArrayList<>();
        int index = koin.size()-1;
        while (jumlah>0){

           while (jumlah - koin.get(index)>=0){
               dompet.add(koin.get(index));
               jumlah = jumlah - koin.get(index);
           }
           index--;
        }
        System.out.println(dompet);
    }
}
