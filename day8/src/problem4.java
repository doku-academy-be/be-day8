import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class problem4 {
    static int BinarySearch(List<Integer> array, int value) {
        int kiri = 0;
        int kanan = array.size() - 1;
        while (kiri <= kanan) {
            int mid = (kiri + kanan) / 2;
            int midVal = array.get(mid);

            if (midVal < value)
                kiri = mid + 1;
            else if (midVal > value)
                kanan = mid - 1;
            else
                return mid;
        }
        return -1;
    }

    public static void main(String[] args) {

        List<Integer> arr = new ArrayList<>(List.of(1, 1, 3, 5, 5, 6, 7));
        System.out.println(arr);
        int x = 3;
        int search = BinarySearch(arr, x);
        System.out.println(search);
    }
}
